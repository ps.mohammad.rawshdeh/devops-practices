FROM openjdk:8-alpine

ARG  PSCI_VERSION=1.0
ENV  VERSION=${PSCI_VERSION}

CMD   printf "║ %-25s │ %-25s ║\n" "devops-app" ${VERSION} >> /VERSION_INFO; \
      printf "╔═══════════════════════════════════════════════════════╗\n"; \
      printf "║                 Version Information                   ║\n"; \
      printf "║ ───────────────────────────────────────────────────── ║\n"; \
      cat /VERSION_INFO; \
      printf "╚═══════════════════════════════════════════════════════╝\n"; \
      chown -R nobody:nobody /VERSION_INFO

COPY   /target/rawashdeh*.jar /app/rawashdeh*.jar
EXPOSE 8070
WORKDIR /app
ENTRYPOINT   java -jar -Dserver.port=8090 -Dspring.profiles.active=h2 *.jar

