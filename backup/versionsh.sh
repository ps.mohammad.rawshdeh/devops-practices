#!bin/bash
unset PART1
unset PART2
git log --date=short | head -3 > versiondate.txt;
PART1=`tail -1 versiondate.txt | sed 's/^.\{10\}//' | sed 's/.\{6\}$//'`
PART1=`echo $PART1;tail -1 versiondate.txt | sed 's/^.\{13\}//' | sed 's/.\{3\}$//'`
PART1=`echo $PART1;tail -1 versiondate.txt | sed 's/^.\{16\}//'`
PART1=$PART1-
git log --date=short | head -1 > versionhash.txt
PART2=`head -1 versionhash.txt | sed 's/^.\{7\}//' | sed 's/.\{32\}$//'`
PART2="$PART1$PART2"
echo $PART2 | tr -d ' ' > versionfinal.txt
echo $PART2 | tr -d ' ' >> ../versionsGenerated.txt
export version=`cat versionfinal.txt`
echo -e "\n\n\e[1;32mpom.xml is generated\e[0m"
echo -e "\n\n\e[1;32mLatest version is listed in the versionsGenerated.txt file\e[0m"
rm ./version*
exit
